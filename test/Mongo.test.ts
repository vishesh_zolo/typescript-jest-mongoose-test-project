import {Document, model, Model, Mongoose, Schema} from "mongoose";
import {PrepMongoose} from "./PrepMongoose";

interface ICat {
  name?: string;
}

interface ICatModel extends ICat, Document {
}

const CatSchema: Schema = new Schema({
  name: String,
});
let Cat: Model<ICatModel>;

beforeEach(async () => {
  const mongoose = new Mongoose();
  Cat = mongoose.model<ICatModel>("Cat", CatSchema);
  await PrepMongoose.prep(mongoose);
});

afterEach(async () => {
  await Cat.db.dropCollection(Cat.collection.name);
});


describe("Mongoose works", () => {

  it("test should be green", async () => {
    const kitty = new Cat({name: 'Zildjian'});
    await kitty.save();
    console.log('meow');
  });

});
