import {Mongoose} from "mongoose";


export class PrepMongoose {

  private static readonly DEBUG = false;

  private static mongoHost() {
    return process.env.CI ? "mongo" : "localhost";
  }

  static async prep(mongoose: Mongoose) {
    mongoose.set("debug", (collectionName: string, methodName: string, ...args: any[]) => {
      if (PrepMongoose.DEBUG) {
        console.log("[Mongo] %s %s %j", collectionName, methodName, args);
      }
    });
    mongoose.connection.on("open", () => {
      if (PrepMongoose.DEBUG) {
        console.log("[Mongo] MongoDB connection opened");
      }
    });
    mongoose.connection.on("connecting", () => {
      if (PrepMongoose.DEBUG) {
        console.log("[Mongo] connecting to MongoDB...");
      }
    });
    mongoose.connection.on("connected", () => {
      if (PrepMongoose.DEBUG) {
        console.info("[Mongo] MongoDB connection connected");
      }
    });
    // Error are thrown when all the autoReconnect process failed
    mongoose.connection.on("error", error => {
      console.error("[Mongo] Error in MongoDb connection fallback relaunch startup connection procedure (error=%j)", error);
      connectMongoose();
    });
    mongoose.connection.on("disconnected", () => {
      console.log("[Mongo] MongoDB connection disconnected");
    });

    let connectMongoose = async () => {
      mongoose.connect('mongodb://' + PrepMongoose.mongoHost() + ':27017/test', {useNewUrlParser: true})
        .then(() => {
          if (PrepMongoose.DEBUG) {
            console.info("[Mongo] connect, onFulfilled");
          }
        })
        .catch((error) => {
          console.error("[Mongo] error connecting to database %j, retrying in 1s", error);
          setTimeout(connectMongoose, 1000);
        });
    };
    await connectMongoose();
  }
}