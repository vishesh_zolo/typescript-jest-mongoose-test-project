import { Document, Schema } from "mongoose";

export interface ISpecies {
  species?: string;
  cry?: string;
  updatesCount?: number;
}

export interface ISpeciesModel extends ISpecies, Document {
}

export const SpeciesSchema: Schema = new Schema({
  species: String,
  cry: String,
  updatesCount: Number
});

export function installVersioningMiddlewareOnSchema(schema: Schema) {
  schema.pre("save", function(next) {
    this.increment();
    return next();
  });
  schema.pre("update", function(next) {
    this.update({}, {$inc: {__v: 1}}, next);
  });
}
